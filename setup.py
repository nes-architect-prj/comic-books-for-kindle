from setuptools import setup, find_packages

def read_requirements():
    with open("requirements.txt", "r") as f:
        return [line.strip() for line in f.readlines() if line.strip() and not line.startswith("#") and not line.startswith("-i")]

setup(
    name="cb4kcli",
    version="1.0.0",
    packages=find_packages(),

    install_requires = read_requirements(),

    entry_points={
        "console_scripts": [
            "cb4kcli = cb4kcli.__main__:main"
        ]
    },

    author="Thomas .D",
    description="Native EPUB Builder for Comics Books",
    url="https://cb4k.nes-architect.fr",

    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent"
    ]
)