from PIL import Image
import os
from shutil import copy, rmtree
from typing import Union
from enum import Enum


class DPTraitment(Enum):
    NOTHING = "NOTHING"
    ROTATE = "ROTATE"
    SPLIT_RTL = "SPLIT_RTL"
    SPLIT_LTR = "SPLIT_LTR"

def cb_convert_img_folder(folder: str, max_size: int = 26214400, quality: int = 50, double_page:DPTraitment=DPTraitment.NOTHING, debug:bool=False):
    """
    Convertit les images dans un dossier en diminuant leur résolution de 10% par itération
    pour que la taille totale des images atteigne la taille maximale spécifiée, tout en appliquant une qualité fixe.
    
    @param folder: Dossier contenant les images.
    @param max_size: Taille maximale totale en octets par défaut 25mo (default 26214400). SI -1, ignore le redimensionnement
    @param quality: Qualité fixe appliquée lors de la sauvegarde des images (default 50).
    """

    if debug: print(f"Starting images compression to reach {round(max_size/1024/1024)} MB")
    
    # Prepare le dossier de conversion
    _converted_dir = folder + ".converted"

    # Function to copy original images to the converted directory by cleaning it up
    def copy_to_converted_dir():
        if os.path.exists(_converted_dir): 
            if debug: print(f"  > {_converted_dir} exists, purging...")
            rmtree(_converted_dir, ignore_errors=True) 
        os.mkdir(_converted_dir)

        for f in sorted(os.listdir(folder)): 
            # Only images
            if f.lower().endswith(('.png', '.jpg', '.jpeg')):
                # If already jpeg file just move it
                if f.lower().endswith(('.jpg', '.jpeg')): copy(os.path.join(folder, f), _converted_dir)
                # if it's a PNG, convert it to JPEG
                if f.lower().endswith('.png'): 
                    file_name = os.path.basename(f).replace(".png",".jpg")
                    to_jpeg = Image.open(os.path.join(folder, f)).convert("RGB")
                    to_jpeg.save(os.path.join(_converted_dir, file_name))
                    if debug: print(f"  > PNG [{f}] converted to JPG")

    # Functions to treat double pages
    def split_double_page(image_path, rtl:bool=True):
        """
        Split double page
        """
        img = Image.open(image_path)
        if img.width > img.height:
            # Find the middle of the page
            middle = img.width / 2
            # Splitted images
            img_l = img.crop((0,0, middle, img.height))
            img_r = img.crop((middle,0, img.width, img.height))
            # Save the images
            base_name, ext = os.path.splitext(image_path)
            filename_r = f"{base_name}{"_1" if rtl else "2"}{ext}"
            filename_l = f"{base_name}{"_2" if rtl else "1"}{ext}"
            img_l.save(filename_l)
            img_r.save(filename_r)
            # delete original double page
            img.close()
            os.remove(path=image_path)
            print(f"    > Double page [{os.path.basename(image_path)}] splitted {'(Right to Left)' if rtl else '(Left to Right)'}")

    def rotate_double_page(image_path):
        """
        Rotate double page
        """
        img = Image.open(image_path)
        if img.width > img.height:
            rotated_img = img.rotate(90, expand=True)
            rotated_img.save(image_path)
            print(f"    > Double page [{os.path.basename(image_path)}] rotated")

    # If double_page different than DPTraitment.NOTHING
    if double_page != DPTraitment.NOTHING:
        for f in sorted(os.listdir(folder)): 
            image_path = os.path.join(folder, f)
            if double_page == DPTraitment.SPLIT_RTL: split_double_page( image_path=image_path )
            if double_page == DPTraitment.SPLIT_LTR: split_double_page( image_path=image_path, rtl=False )
            if double_page == DPTraitment.ROTATE: rotate_double_page( image_path=image_path )

    # Initial copy of original images
    copy_to_converted_dir()
    
    # If max_size = -1 we skip the compression and the resize of the images
    if max_size == -1: return  

    # Fonction pour obtenir la taille totale des images
    def get_total_size():
        return sum(os.path.getsize(os.path.join(_converted_dir, f)) for f in sorted(os.listdir(_converted_dir)))
    
    # Si la taille totale est déjà sous la limite, pas de traitement nécessaire
    if get_total_size() <= max_size:
        print(f"    > No need to compress, already under the specified limit")
        return
    
    # Diminuer la résolution par étapes jusqu'à atteindre la taille limite
    reduction_factor = 0.95  # Réduction de 10% par itération
    reduction_step = 0.05
    resized = False

    while get_total_size() > max_size:
        for image_file in [f for f in os.listdir(_converted_dir) if f.lower().endswith(('.png', '.jpg', '.jpeg'))]:
            img_filename = os.path.join(_converted_dir, image_file)
            img = Image.open(img_filename)

            # Réduire la résolution
            new_width = int(img.width * reduction_factor)
            new_height = int(img.height * reduction_factor)
            img = img.resize((new_width, new_height), Image.Resampling.LANCZOS)

            # Enregistrer l'image avec la qualité fixe donnée
            img.save(img_filename, optimize=True, quality=quality)
            resized = True
            # print(f"Réduction de {image_file} à {new_width}x{new_height} avec qualité={quality}")

        # Vérifier la taille totale après une passe de réduction
        total_size = get_total_size()
        if debug: print(f"Still compressing : {total_size / 1024 / 1024:.2f} Mo > {max_size / 1024 / 1024:.2f}")

        # Si la taille totale est maintenant sous la limite, arrêter la boucle
        if total_size <= max_size:
            break

        # If the size is always greater than the max one, reset the folder
        # Lower the reductor factror
        reduction_factor -= reduction_step
        # print(f"Nouveau facteur de dimensionnement : x{round(reduction_factor,ndigits=2)}")
        copy_to_converted_dir()

    if resized:
        if debug: print(f"Final size after compression : {get_total_size() / 1024 / 1024:.2f} Mo")
    else:
        print("Impossible d'atteindre la taille souhaitée sans une compression supplémentaire.")
