import os
import shutil
from typing import List
import PIL
import PIL.Image
from lxml import etree
import uuid
import zipfile

class Epub:
    def __init__(self, image_folder:str, temp_folder:str = "./tmp") -> None:
        
        # Define properties of the ebook
        self.title : str = None
        self.isbn : str = None
        self.uuid : str = str(uuid.uuid4())
        self.lang : List[str] =  ['fr','fr-FR']
        self.publisher : str = "Comic Book For Kindle"
        self.author : str = "Comic Book For Kindle"
        
        # Define the chapter
        self.chapters : List = []

        # Define EPUB DIR CONST
        self.DIR_TEMP : str = temp_folder
        self.DIR_BUILD : str = os.path.join(self.DIR_TEMP, "build")
        self.DIR_META_INF : str = os.path.join(self.DIR_BUILD, 'META-INF')
        self.DIR_OPS : str = os.path.join(self.DIR_BUILD, 'OPS')
        self.DIR_OPS_IMAGES : str = os.path.join(self.DIR_OPS, 'images')
        self.DIR_OPS_CSS : str = os.path.join(self.DIR_OPS, 'css')
        self.DIR_OPS_XHTML : str = os.path.join(self.DIR_OPS, 'xhtml')

        # Init the Epub structure
        self.__init_structure()

        # Move images
        self.__move_images_to_build(image_files=[os.path.join(image_folder, file) for file in sorted(os.listdir(image_folder))])

        # Define the resolution of the Page's viewport from the first image, else used the widht/height value pass in args
        # dim = PIL.Image.open(os.path.join(self.DIR_OPS_IMAGES, os.listdir(self.DIR_OPS_IMAGES)[0]))
        # self.IMG_WIDTH = width if width > -1 else dim.width
        # self.IMG_HEIGHT = height if height > -1 else dim.height
        # dim.close()

    def __init_structure(self):
        """
        Create the Folders of the structuration of en EPUB3
        """
        if os.path.exists(self.DIR_BUILD): shutil.rmtree(self.DIR_BUILD, ignore_errors=True)
        else: 
            os.makedirs(self.DIR_BUILD, exist_ok=True)
            os.makedirs(self.DIR_META_INF, exist_ok=True)
            os.makedirs(self.DIR_OPS, exist_ok=True)
            os.makedirs(self.DIR_OPS_IMAGES, exist_ok=True)
            os.makedirs(self.DIR_OPS_CSS, exist_ok=True)
            os.makedirs(self.DIR_OPS_XHTML, exist_ok=True)
            # Create the mimetype file
            with open( os.path.join(self.DIR_BUILD, "mimetype"), "w" ) as mime_file:
                mime_file.write("application/epub+zip")
            # Create the empty default css file
            # with open( os.path.join(self.DIR_OPS_CSS, "default.css"), "w" ) as css_file:
            #     css_file.write("")

    def __move_images_to_build(self, image_files:str):
        """
        Move All images into the build/oebps/images folder of the ebook
        """
        for idx, image in enumerate(image_files):
            # If it's the first image, we will copy it and name the copy cover.{ext}
            if idx == 0: shutil.copy(image, os.path.join(self.DIR_OPS_IMAGES, f"cover{os.path.splitext(image)[1]}"))
            # Move normally the other images
            shutil.move(image, os.path.join(self.DIR_OPS_IMAGES, f"page_{str(idx).rjust(4,"0")}{os.path.splitext(image)[1]}"))

    def build(self):
        # Generate all chapters
        [self.__add_chapter(image_file=image) for image in sorted(os.listdir(self.DIR_OPS_IMAGES))]

        # Generate the cover page
        self.__add_chapter(image_file=[cover for cover in os.listdir(self.DIR_OPS_IMAGES) if cover.startswith("cover.")][0], is_cover=True)

        # Generate the about page and add it to chapter
        self.__create_about()

        # Generate manifest
        self.__create_package()

        # Generate the toc and container
        self.__create_toc_xhtml()
        self.__create_toc_ncx()
        self.__create_container()

    # def __create_about(self):
    #     """
    #     Create page as "About CBF4" and add it to chapters
    #     """
    #     page_filename = os.path.join(self.DIR_OPS_XHTML,"page_about.xhtml")

    #     xhtml = etree.Element("html", nsmap={
    #         None: "http://www.w3.org/1999/xhtml",
    #         "xml": "http://www.w3.org/XML/1998/namespace", 
    #         "epub": "http://www.idpf.org/2007/ops",
    #         "lang": "fr", 
    #     })

    #     head = etree.SubElement(xhtml, "head")
    #     etree.SubElement(head, "title").text = "A propos de CB4K"

    #     # Define the image tag with the resolution of the image
    #     etree.SubElement(head, "meta", attrib={
    #         "name": "viewport",
    #         "content": "width=600, height=800"
    #     })

    #     body = etree.SubElement(xhtml, "body")
    #     etree.SubElement(body, "h2").text = "A propos de CB4K"
    #     etree.SubElement(body, "p").text = "Ce livre à été converti depuis un CBR/CBZ vers EPUB natif via Comic Book For Kindle (CB4K)"
    #     etree.SubElement(body, "p").text = "Logiciel disponible ici https://cb4k.nes-architect.fr"

    #     xhtml_string = etree.tostring(
    #         element_or_tree=xhtml,
    #         xml_declaration=True,
    #         pretty_print=True,
    #         encoding="UTF-8"
    #     )

    #     with open(page_filename, "b+w") as file:
    #         file.write(xhtml_string)

    def __create_about(self):
        """
        Create page as "About CB4K" and add it to chapters
        """
        page_filename = os.path.join(self.DIR_OPS_XHTML, "page_about.xhtml")

        xhtml = etree.Element("html", nsmap={
            None: "http://www.w3.org/1999/xhtml",
            "xml": "http://www.w3.org/XML/1998/namespace", 
            "epub": "http://www.idpf.org/2007/ops",
            "lang": "fr", 
        })

        # HEAD
        head = etree.SubElement(xhtml, "head")
        etree.SubElement(head, "title").text = "À propos de CB4K"

        # Meta viewport 
        etree.SubElement(head, "meta", attrib={
            "name": "viewport",
            "content": "width=600, height=800"
        })

        # Style CSS 
        style = etree.SubElement(head, "style")
        style.text = """
            body {
                font-family: Arial, sans-serif;
                text-align: center;
                padding: 20px;
                background-color: #f4f4f4;
            }
            .container {
                max-width: 600px;
                margin: auto;
                background: white;
                padding: 20px;
                border-radius: 10px;
                box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
            }
            h2 {
                color: #333;
            }
            p {
                font-size: 1.2em;
                color: #555;
            }
            .highlight {
                font-weight: bold;
                color: #0077cc;
            }
            .footer {
                margin-top: 20px;
                font-size: 0.9em;
                color: #777;
            }
        """

        # BODY
        body = etree.SubElement(xhtml, "body")
        container = etree.SubElement(body, "div", attrib={"class": "container"})

        etree.SubElement(container, "h2").text = "À propos de CB4K"
        etree.SubElement(container, "p").text = "Ce livre a été converti depuis un CBR/CBZ/PDF vers EPUB 3.3 natif via Comic Book For Kindle (CB4K)."
        
        link = etree.SubElement(container, "p")
        link_text = etree.SubElement(link, "a", attrib={"href": "https://cb4k.nes-architect.fr", "class": "highlight"})
        link_text.text = "Disponible sur cb4k.nes-architect.fr"

        footer = etree.SubElement(container, "p", attrib={"class": "footer"})
        footer.text = "Merci d'utiliser CB4K pour vos conversions !"

        # XHTML
        xhtml_string = etree.tostring(
            element_or_tree=xhtml,
            xml_declaration=True,
            pretty_print=True,
            encoding="UTF-8"
        )

        with open(page_filename, "wb") as file:
            file.write(xhtml_string)


    def __create_cover(self):
        """
        Create the cover image from the first image by default
        """
        pass

    def set_cover(self, image_file:str):
        """
        Define the cover of the book based on the image
        @param image_file (str): The path to the cove image 
        """
        pass

    def __add_chapter(self, image_file:str, is_cover:bool=False):
        """
        Add a new chapter to the ebook based on the image
        """
        
        page_number = str(len(self.chapters)+1).rjust(4,"0")
        page_filename = os.path.join(self.DIR_OPS_XHTML, f"page_cover.xhtml") if is_cover else os.path.join(self.DIR_OPS_XHTML, f"page_{page_number}.xhtml")
        
        xhtml = etree.Element("html", nsmap={
            None: "http://www.w3.org/1999/xhtml",
            "xml": "http://www.w3.org/XML/1998/namespace", 
            "epub": "http://www.idpf.org/2007/ops",
            "lang": "fr", 
        })

        head = etree.SubElement(xhtml, "head")
        title = etree.SubElement(head, "title")
        title.text = "Cover" if is_cover else f"Page {page_number}"

        
        # Define the resolution of the Page's viewport from the first image, else used the widht/height value pass in args
        img_resolution = PIL.Image.open(os.path.join(self.DIR_OPS_IMAGES, os.path.basename(image_file)))
        
        # Define the image tag with the resolution of the image
        etree.SubElement(head, "meta", attrib={
            "name": "viewport",
            "content": f"width={img_resolution.width}, height={img_resolution.height}"
        })

        body = etree.SubElement(xhtml, "body", {"{http://www.idpf.org/2007/ops}type":"bodymatter"})
        etree.SubElement(body, "img", attrib={
            "src": f"../images/{os.path.basename(image_file)}",
            "alt": "Cover" if is_cover else f"Page {page_number}",
        })

        xhtml_string = etree.tostring(
            element_or_tree=xhtml,
            xml_declaration=True,
            pretty_print=True,
            encoding="UTF-8"
        )

        self.chapters.append( xhtml_string )
        with open(page_filename, "b+w") as chapter_file:
            chapter_file.write(xhtml_string)

    def __create_toc_xhtml(self):
        """
        Create table of content of the book the ncx and xhtml files
        """

        page_filename_xhtml = os.path.join(self.DIR_OPS_XHTML, "page_toc.xhtml")

        # Define namespaces
        NSMAP= {
            None: "http://www.w3.org/1999/xhtml",
            "epub": "http://www.idpf.org/2007/ops"
        }

        # The xhtml file# Pacakge info
        toc = etree.Element("html", nsmap=NSMAP)

        # The head
        head = etree.SubElement(toc, "head")
        # The title
        etree.SubElement(head, "title").text = self.title
        # The css file
        # etree.SubElement(head, "link", attrib={"rel":"../css/default.css", "type":"text/css"})

        # Body
        body = etree.SubElement(toc, "body", {"{http://www.idpf.org/2007/ops}type":"formatter"})

        # Header
        header = etree.SubElement(body, "header")
        etree.SubElement(header, "h1").text = "Table of Contents"

        # Nav
        nav = etree.SubElement(body, "nav", {"{http://www.idpf.org/2007/ops}type": "toc"})
        ol = etree.SubElement(nav, "ol")
        # The beginning of the book
        li = etree.SubElement(ol, "li", {"{http://www.idpf.org/2007/ops}type": "chapter", "id":"toc-01"})
        etree.SubElement(li, "a", {"href": "page_0001.xhtml"}).text = self.title
        # The about Chapter
        li = etree.SubElement(ol, "li", {"{http://www.idpf.org/2007/ops}type": "chapter", "id":"toc-about"})
        etree.SubElement(li, "a", {"href":"page_about.xhtml"}).text = f"A propos de CB4K"
        
        # Page list
        nav_pages = etree.SubElement(body, "nav", {"{http://www.idpf.org/2007/ops}type": "page-list"})
        ol = etree.SubElement(nav_pages, "ol")
        for idx, page in enumerate(sorted(os.listdir(self.DIR_OPS_XHTML))):
            li = etree.SubElement(ol, "li")
            etree.SubElement(li, "a", {"href": page}).text = str(idx+1)

        # Write the file
        xhtml_string = etree.tostring(
            element_or_tree=toc,
            xml_declaration=True,
            pretty_print=True,
            encoding="UTF-8"
        )

        with open(page_filename_xhtml, "b+w") as file:
            file.write(xhtml_string)

    def __create_toc_ncx(self):
        """
        Create table of content of the book the ncx and xhtml files
        """

        page_filename_ncx = os.path.join(self.DIR_OPS_XHTML, "toc.ncx")

        # Define namespaces
        NSMAP= {
            None: "http://www.w3.org/1999/xhtml",
            "ncx": "http://www.daisy.org/z3986/2005/ncx/"
        }

        # The xhtml file# Pacakge info
        ncx = etree.Element("{http://www.daisy.org/z3986/2005/ncx/}ncx", attrib={"version": "2005-1"}, nsmap=NSMAP)

        # The head
        head = etree.SubElement(ncx, "{http://www.daisy.org/z3986/2005/ncx/}head")
        etree.SubElement(head, "{http://www.daisy.org/z3986/2005/ncx/}meta", {"name":"dtb:uuid", "content": f"urn:uuid:{self.uuid}"})
        etree.SubElement(head, "{http://www.daisy.org/z3986/2005/ncx/}meta", {"name":"depth", "content": "-1"})
        etree.SubElement(head, "{http://www.daisy.org/z3986/2005/ncx/}meta", {"name":"totalPageCount", "content": "0"})
        etree.SubElement(head, "{http://www.daisy.org/z3986/2005/ncx/}meta", {"name":"maxPageNumber", "content": "0"})

        # Doc title
        doc_title = etree.SubElement(ncx, "{http://www.daisy.org/z3986/2005/ncx/}docTitle")
        etree.SubElement(doc_title, "{http://www.daisy.org/z3986/2005/ncx/}text").text = self.title
        
        # Doc Author
        doc_author = etree.SubElement(ncx, "{http://www.daisy.org/z3986/2005/ncx/}docAuthor")
        etree.SubElement(doc_author, "{http://www.daisy.org/z3986/2005/ncx/}text").text = self.author

        # NAv Map - first page
        nav_map = etree.SubElement(ncx, "{http://www.daisy.org/z3986/2005/ncx/}navMap")
        point = etree.SubElement(nav_map, "{http://www.daisy.org/z3986/2005/ncx/}navPoint", {"id": "page_0001", "playOrder": "1"})
        label = etree.SubElement(point, "{http://www.daisy.org/z3986/2005/ncx/}navLabel")
        etree.SubElement(label, "{http://www.daisy.org/z3986/2005/ncx/}text").text = self.title
        etree.SubElement(point, "{http://www.daisy.org/z3986/2005/ncx/}content", {"src": "page_0001.xhtml"})

        # NAv Map - About page
        point = etree.SubElement(nav_map, "{http://www.daisy.org/z3986/2005/ncx/}navPoint", {"id": "page_about", "playOrder": "2"})
        label = etree.SubElement(point, "{http://www.daisy.org/z3986/2005/ncx/}navLabel")
        etree.SubElement(label, "{http://www.daisy.org/z3986/2005/ncx/}text").text = "A propos de CB4K"
        etree.SubElement(point, "{http://www.daisy.org/z3986/2005/ncx/}content", {"src": "page_about.xhtml"})

        # Write the file
        xhtml_string = etree.tostring(
            element_or_tree=ncx,
            xml_declaration=True,
            pretty_print=True,
            encoding="UTF-8"
        )

        with open(page_filename_ncx, "b+w") as file:
            file.write(xhtml_string)

    def __create_package(self):
        """ 
        Create the package file to describe the book (opf file)
        """
        page_filename = os.path.join(self.DIR_OPS, "package.opf")

        # Namespaces
        NSMAP= {
            None: "http://www.idpf.org/2007/opf",
            "lang": self.lang[0],
            "opf": "http://www.idpf.org/2007/opf",
            "dc": "http://purl.org/dc/elements/1.1/",
            "dcterms": "http://purl.org/dc/terms/"
        }
        
        # Pacakge info
        package = etree.Element("package", nsmap=NSMAP, attrib={
            "unique-identifier": "bookid",
            "version": "3.0",
            "prefix": "rendition: http://www.idpf.org/vocab/rendition/#"
        })

        # Metadata info
        metadata = etree.SubElement(package, "metadata", nsmap=NSMAP)

        # Metadata about the book
        etree.SubElement(metadata, "{http://purl.org/dc/elements/1.1/}title").text = self.title
        etree.SubElement(metadata, "{http://purl.org/dc/elements/1.1/}creator").text = self.author
        etree.SubElement(metadata, "{http://purl.org/dc/elements/1.1/}identifier", attrib={"id": "bookid"}).text = self.uuid
        etree.SubElement(metadata, "{http://purl.org/dc/elements/1.1/}publisher").text = self.publisher
        etree.SubElement(metadata, "{http://purl.org/dc/elements/1.1/}language").text = self.lang[1]

        etree.SubElement(metadata, "meta", {"property":"rendition:layout"}).text = "pre-paginated"
        etree.SubElement(metadata, "meta", {"property":"rendition:orientation"}).text = "auto"
        etree.SubElement(metadata, "meta", {"property":"rendition:spread"}).text = "auto"

        etree.SubElement(metadata, "meta", {"property":"dcterms:modified"}).text = "2024-12-03T00:00:00Z"


        # Metadata (manifest section) about other pages
        manifest = etree.SubElement(package, "manifest")
        # etree.SubElement(manifest, "item", attrib={"id":"default-style", "href": "css/default.css", "media-type": "text/css"})
        # etree.SubElement(metadata, "item", attrib={"id":"en-style", "href": "css/en.css", "media-type": "text/css"})
        # etree.SubElement(metadata, "item", attrib={"id":"fr-style", "href": "css/fr.css", "media-type": "text/css"})

        # Manifest off all images and put good mimetype especially for jpeg/jpg
        for image_file in sorted(os.listdir(self.DIR_OPS_IMAGES)):
            mimetype = ""
            ext = os.path.splitext(image_file)[1]
            if ext in [".jpg", ".jpeg"]: mimetype = "image/jpeg"
            if ext in [".png"]: mimetype = "image/png"
            if ext in [".gif"]: mimetype = "image/gif"
            # Define the attributes
            attrib={
                "id": f"{os.path.splitext(image_file)[0]}_img",
                "href":f"images/{image_file}",
                "media-type": mimetype
            }
            # If it's the cover, add new properties
            if os.path.splitext(image_file)[0] == "cover":
                attrib["properties"] = "cover-image"

            etree.SubElement(manifest, "item", attrib=attrib)

        # Manifest about and toc
        # etree.SubElement(manifest, "item", attrib={"id":"page_about", "href": "xhtml/page_about.xhtml", "media-type":"application/xhtml+xml"})
        etree.SubElement(manifest, "item", attrib={"id":"toc", "href": "xhtml/page_toc.xhtml", "properties": "nav", "media-type":"application/xhtml+xml"})

        # Manifest for all chapters (pages)
        for page_file in sorted(os.listdir(self.DIR_OPS_XHTML)):
            attrib = {
                "id": f"{os.path.splitext(page_file)[0]}_xhtml",
                "href":f"xhtml/{page_file}",
                "media-type": "application/xhtml+xml"
            }
            

            # If it's the about xhtml page, change his ID
            if os.path.splitext(page_file)[0] == "page_about":
                attrib["id"] = "about"

            # If it's the table of content xhtml page, change his ID
            if os.path.splitext(page_file)[0] == "page_toc":
                attrib["id"] = "toc"

            etree.SubElement(manifest, "item", attrib=attrib)

        # Manifest of the ncxtoc
        etree.SubElement(manifest, "item", attrib={"id": "ncxtoc", "href":"xhtml/toc.ncx", "media-type": "application/x-dtbncx+xml"})

        # Spine section
        spine = etree.SubElement(package, "spine", attrib={
            "toc": "ncxtoc",
            # "page-progression-direction": "rtl"
        })
        rtl = True
        for page_file in sorted(os.listdir(self.DIR_OPS_XHTML)):
            
            attrib = {
                "idref": f"{os.path.splitext(page_file)[0]}_xhtml",
                # "properties": "page-spread-left" if rtl else "page-spread-right"
            }

            # If it's the about xhtml page, change his ID
            if os.path.splitext(page_file)[0] == "page_about":
                attrib["idref"] = "about"
            
            etree.SubElement(spine, "itemref", attrib=attrib)
            rtl = not rtl

        # Write the file
        xhtml_string = etree.tostring(
            element_or_tree=package,
            xml_declaration=True,
            pretty_print=True,
            encoding="UTF-8"
        )

        with open(page_filename, "b+w") as file:
            file.write(xhtml_string)

    def __create_container(self):
        """ 
        Create the container.xml file
        """
        page_filename = os.path.join(self.DIR_META_INF, "container.xml")

        # Namespaces
        NSMAP= {
            None: "urn:oasis:names:tc:opendocument:xmlns:container"
        }
        
        # Container info
        container = etree.Element("container", nsmap=NSMAP, attrib={"version": "1.0"})
        rootfiles = etree.SubElement(container, "rootfiles")
        etree.SubElement(rootfiles, "rootfile", {"full-path": "OPS/package.opf", "media-type":"application/oebps-package+xml"})

        # Write the file
        xhtml_string = etree.tostring(
            element_or_tree=container,
            xml_declaration=True,
            pretty_print=True,
            encoding="UTF-8"
        )

        with open(page_filename, "b+w") as file:
            file.write(xhtml_string)