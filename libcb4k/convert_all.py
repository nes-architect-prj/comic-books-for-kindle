import os
import libcb4k
import libcb2tg
import libcbmail
import shutil
from typing import Union


def convert_all(path:str=os.getcwd(), output:str=os.getcwd(), max_size:int=-1, quality:int=80, debug:bool=False, save_2_tg:bool=False, send_mail:bool=False):
    """
    Convert all CBZ/CBR/PDF files from the specified folder
    """

    TMP_FOLDER="./tmp.cb4k"

    # Cleanup the work folder
    if os.path.exists(TMP_FOLDER): shutil.rmtree(TMP_FOLDER, ignore_errors=True)
    
    # get all CBZ/CBR/PDF files
    for file in [entry for entry in os.listdir(path=path) if entry.endswith( ('.cbz', '.cbr', '.pdf') )]:
        if debug : print(f"-> Converting {file} ...")

        # Extract the images
        libcb4k.cb_extract(cbfile=file, output=os.path.join(TMP_FOLDER, "images"), debug=debug)

        # Compress the images
        libcb4k.cb_convert_img_folder(folder=os.path.join(TMP_FOLDER, "images"), max_size=max_size, quality=quality, debug=debug)

        # Create the EPUB object and specify the images folder
        epub = libcb4k.Epub(image_folder=os.path.join(TMP_FOLDER, "images.converted"), temp_folder=TMP_FOLDER)

        # Define the info of the ebook
        epub.title = os.path.splitext( os.path.basename(file) )[0]

        # Build all contents (chapters, xml files...)
        epub.build()

        # Create the zipped EPUB
        libcb4k.cb_compress(epub=epub, output_folder=output)

        # Path for the EPUB file built
        epub_file = os.path.join(output, epub.title + ".epub")

        # save to TG ?
        if save_2_tg :
            # First remove file if exists
            file_exists = libcb2tg.bot.filemgr.filedb.find_save_file(file_name=epub.title + ".epub")
            if file_exists is not None:
                if debug: print("   ... exists, removing old one")
                libcb2tg.bot.filemgr.delete(file_id=file_exists.get("file_id"))

            # send
            if debug : print("  ... sending to TG")
            libcb2tg.bot.filemgr.upload(file=epub_file)

        # Send to mail ?
        if send_mail :
            # if max_size > 25 * 1024 * 1024:
            #     print(f" ... can't send by mail (MAX SIZE EXCEED)") 
            # else:
            if debug : print(f" ... sending by mail")
            libcbmail.send(file_path=epub_file, debug=debug)

    if debug : print("Convert done")

