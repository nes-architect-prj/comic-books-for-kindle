import os
import zipfile
import shutil
from .epub import Epub


def cb_compress(epub: Epub, output_folder:str = os.curdir, clean_dirs_when_done:bool = False):
    # Output filename
    filename = os.path.join(output_folder,f"{epub.title}.epub")

    # Check the mimetype file
    mimetype_file = os.path.join(epub.DIR_BUILD, "mimetype")
    if not os.path.exists( mimetype_file ):
        raise FileExistsError("Le fichier mimetype est introuvable !")

    with zipfile.ZipFile(filename, "w", compression=zipfile.ZIP_DEFLATED) as epub_zip:
        # Create the zip file and add first the mimetype file
        epub_zip.write( os.path.join(mimetype_file), arcname="mimetype", compress_type=zipfile.ZIP_STORED )
        
        # Browse all files in the build folder
        for root, _, files in os.walk( epub.DIR_BUILD ):
            for file in files:
                
                # Define the pathname of the current file
                file_path = os.path.join(root, file)
                
                # Ignore mimetype because already added

                if os.path.normpath(file_path) == os.path.normpath(mimetype_file):
                    continue
                
                # Add other files (generate rel path from the BUILD folder as root)
                arcname = os.path.relpath(file_path, start=epub.DIR_BUILD)
                epub_zip.write(file_path, arcname=arcname)
        
        # Clean working dir
        shutil.rmtree(epub.DIR_TEMP, ignore_errors=True)