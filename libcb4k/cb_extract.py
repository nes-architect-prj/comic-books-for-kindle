from pdf2image import convert_from_path, convert_from_bytes
import zipfile
import rarfile
import os
import shutil
from PIL import Image


KINDLE_HEIGHT=1448

def cb_extract(cbfile: str, output: str, debug:bool=False):
    """
    Extrait un fichier ZIP/RAR ou PDF dans un dossier spécifique.
    
    @param cbfile: Chemin vers le fichier ZIP ou RAR à extraire.
    @param output: Dossier cible où les fichiers seront extraits.
    """
    
    # Create the folder if not exists
    if os.path.exists(output): shutil.rmtree(output, ignore_errors=True) 
    else: os.makedirs(output, exist_ok=True)

    # Extract CBZ
    if zipfile.is_zipfile(cbfile):
        if debug: print("Extracting cbz")
        with zipfile.ZipFile(cbfile,"r") as zip_ref:
            for entry in zip_ref.infolist():
                if not entry.is_dir():
                    entry.filename = os.path.basename(entry.filename)
                    zip_ref.extract(entry, output)

    # Extract CBR
    elif cbfile.lower().endswith(".cbr"):
        if debug: print("Extracting cbr")
        with rarfile.RarFile(cbfile,"r") as rar_ref:
            for entry in rar_ref.infolist():
                if not entry.filename.endswith("/"):
                    with rar_ref.open(entry) as f:
                        img = Image.open(f, "r")
                        img.save(os.path.join(output, os.path.basename(entry.filename)) )                            

    # Extract PDF with subtility
    elif cbfile.lower().endswith(".pdf"):
        if debug: print("Extracting PDF")

        # To avoid the stretching of images we must calculate the Ratio between the
        # kindle pixels height and the original Pdf image size to determinate the right width with Kindle height
        # 
        # PDF are often bigger images, so the refactor it's to fit the Kindle resolution before the final of compression by CB4K

        # Get the first image of the PDF it will be used for ref
        pdf_first_image = convert_from_path(pdf_path=cbfile, first_page=0, last_page=1, fmt="jpeg").pop()
        if debug: print(f"Images resolution are {pdf_first_image.width} x {pdf_first_image.height}" )

        # Get the ratio between Kindle height and the image height, only of it's greater than kindle one's
        ratio = 1
        if pdf_first_image.height > KINDLE_HEIGHT:
            ratio = pdf_first_image.width / pdf_first_image.height
        
        # Determinate the height/width with the ratio
        target_height = pdf_first_image.height if ratio == 1 else KINDLE_HEIGHT
        target_width = pdf_first_image.width if ratio == 1 else round(KINDLE_HEIGHT * ratio)

        if debug and ratio != 1: 
            print(f"The PDF image is bigger than the Kindle resolution, reduce resolution with ratio by {round(ratio,2)}")
            print(f"    ... new resolution is {target_width} x {target_height}")

        # Extract images
        extracted_images = convert_from_path(
            pdf_path=cbfile, 
            output_folder=output,
            fmt="jpeg",
            output_file="",
            grayscale=True,
            size=(target_width,target_height),
            thread_count=os.cpu_count()
        )

    else:
        raise Exception(f"Not an CBZ or CBR archive file : {cbfile}")
    
    # Check if we have extracted some images files (if 0, raise exception)
    if len( os.listdir(output) ) == 0:
        raise Exception("No Images files extracted ! Please check your CB archive")

    if debug: print("Extract done")
