import os
import json
import requests


CONFIG_FILE = os.path.join( os.path.expanduser("~"), ".cb4k_cb2tg")
URI = f"https://api.telegram.org/bot"

def save_credentials(token, chat_id):
    """
    Save token and chat_id
    """
    data =  {
        "token": token,
        "chat_id": chat_id
    }

    with open(CONFIG_FILE, "w") as file:
        json.dump(data, file, indent=4)

def load_credentials(to_dict:bool=False):
    """
    Load credentials from json file
    """
    try:
        with open(CONFIG_FILE, "r") as file:
            data = json.load(file)
            return data if to_dict else (data["token"], data["chat_id"])
    except FileNotFoundError:
        print(f"Config file {CONFIG_FILE} not found !")
        return {} if to_dict else (None, None)
    except KeyError:
        print(f"File {CONFIG_FILE} is corrupted or not a json")

def test_credentials():
    """
    Test creds with a request
    """
    # Load credentials
    token, chat_id = load_credentials()
    if not token or not chat_id:
        exit("Error: No TOKEN or CHAT_ID defined !")

    response = requests.get(f"{URI}{token}/getUpdates")
    if response.status_code == 200:
        if json.loads(response.text)["ok"]:
            return True
        else:
            exit("Error")
    else:
        exit("Can't access to TG API with these token and chat_id")