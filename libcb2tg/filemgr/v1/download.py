import requests
import os
from libcb2tg import creds, filemgr
from pathlib import Path


HOME = os.path.join( os.path.expanduser("~") )

def download(file_id:str, output_path:str|None=None):
    """
    Download a file from the chat of the BOT
    """
    # Load credentials
    token, chat_id = creds.load_credentials()
    if not token or not chat_id:
        exit("Error: No TOKEN or CHAT_ID defined !")

    # Build the root URI
    root_uri =  f"https://api.telegram.org"

    # Build the URI to retrive the download URL
    url = f"{root_uri}/bot{token}/getFile?file_id={file_id}"
    
    # Define the output path with default filen_name from DB if not defined
    if output_path is None:
        output_path = os.path.join({HOME}, f"{filemgr.v1.filedb.find_save_file_from_id(file_id=file_id).get('file_name')}")
    else:
        output_path = Path(output_path).expanduser()

    # Download the file_path
    response = requests.get(url=url)
    if response.status_code == 200:
        
        # Retrieve the path of the file_id
        file_path = response.json().get("result").get("file_path")
        
        # Build the download URL
        download_url = f"{root_uri}/file/bot{token}/{file_path}"

        # Download the file and save it
        file_response = requests.get(url=download_url)
        if file_response.status_code == 200:
            with open(output_path, "wb") as file:
                file.write(file_response.content)
                return True
        else:
            print(f"Error when saving file id {file_id}")
            return False
    else:
        print(f"Error when downloading file id {file_id} : {response.text}")
        return False
    