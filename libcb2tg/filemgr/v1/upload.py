import requests
from typing import Union
import os
from libcb2tg import creds, filemgr


def upload(file) -> Union[{}, None]:
    """
    Upload a file to the chat of the BOT
    """
    # Load credentials
    token, chat_id = creds.load_credentials()
    if not token or not chat_id:
        exit("Error: No TOKEN or CHAT_ID defined !")

    # Build the URI
    url = f"https://api.telegram.org/bot{token}/sendDocument"

    # define the filename
    file_name = os.path.basename(file).replace("'"," ")

    # Check if the file doesn't exists in DB before
    file_exists = filemgr.v1.filedb.find_save_file(file_name=file_name)
    if file_exists is not None:
        print(f"Error: file \"{file_name}\" already exists on the chat, change his name")
        return False

    # Send the file
    with open(file, 'rb') as file:
        files = {"document": file}
        data = {"chat_id": chat_id, "caption": f"Voici votre EPUB : {file_name}"}

        response = requests.post(url=url, data=data, files=files)

        if response.status_code == 200:
            document = response.json().get("result").get("document")
            message_id = response.json().get("result").get("message_id")
            return filemgr.v1.filedb.save_file_id(document=document, message_id=message_id)
        else:
            print(response.status_code)
            print(response.text)
            return None
    