import os
import json
import datetime
from typing import Union


DB_FILE = os.path.join( os.path.expanduser("~"), ".cb4k_cb2tg_db")

def save_file_id(document:any, message_id:str):
    """
    Save info of a sent file to TG
    """
    
    # Define the current saved files
    history = []
    if os.path.exists(DB_FILE):
        with open(DB_FILE, "r") as file:
            history = json.load(file)

    # Append the new file
    new_entry = {
        "file_name": document.get("file_name"),
        "file_id": document.get("file_id"),
        "file_mime": document.get("mime_type"),
        "file_size": document.get("file_size"),
        "file_date": float(datetime.datetime.now().timestamp()),
        "message_id": str(message_id)
    }

    history.append(new_entry)

    # Save all into the DB
    with open(DB_FILE, "w") as file:
        json.dump(history, file, indent=4)
    
    return new_entry

def find_save_file(file_name:any) -> Union[{},None]:
    """
    Search and return a file info from a file_name
    """

    # Define the current saved files
    history = []
    if os.path.exists(DB_FILE):
        with open(DB_FILE, "r") as file:
            history = json.load(file)

    file = [file for file in history if file.get("file_name") == file_name]

    if len(file) == 1:
        return file[0]
    else:
        return None 

def find_save_file_from_id(file_id:str) -> Union[{},None]:
    """
    Search and return a file info from a file_id
    """

    # Define the current saved files
    history = []
    if os.path.exists(DB_FILE):
        with open(DB_FILE, "r") as file:
            history = json.load(file)

    file = [file for file in history if file.get("file_id") == file_id]
    if len(file) == 1:
        return file[0]
    else:
        return None 

def list_saved_files():
    """
    List all files from the local DB
    """

    # Define the current saved files
    history = []
    if os.path.exists(DB_FILE):
        with open(DB_FILE, "r") as file:
            history = json.load(file)

    return history

def delete_file_id(file_id):
    """
    Delete file info from the DB
    """

    # File exists in DB ?
    file_db = find_save_file_from_id(file_id=file_id)
    if file_db is not None:
        with open(DB_FILE, mode="r") as file:
            history = json.load(file)
            new_history = [entry for entry in history if entry.get("file_name") != file_db.get("file_name")]
        with open(DB_FILE, mode="w") as file:
            json.dump(new_history, file, indent=4)    
        return True
    else:
        print("Error : this id didn't exists")
        return False