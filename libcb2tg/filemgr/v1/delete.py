import requests
import os
from libcb2tg import creds, filemgr


def delete(file_id:str):
    """
    Delete a file from the chat of the BOT
    """
    # Load credentials
    token, chat_id = creds.load_credentials()
    if not token or not chat_id:
        exit("Error: No TOKEN or CHAT_ID defined !")

    # Retrieve the file
    file = filemgr.v1.filedb.find_save_file_from_id(file_id=file_id)
    if file is None:
        print("File didn't exists on DB")
        return False

    # Build the uri
    url =  f"https://api.telegram.org/bot{token}/deleteMessage"
    data = {"chat_id": chat_id, "message_id": file.get("message_id")}

    # Delete the message with his content
    response = requests.post(url=url, data=data)
    if response.status_code == 200:
        filemgr.v1.filedb.delete_file_id(file_id=file_id)
        return True
    else:
        print("Error: Can't delete message from telegram...")
        return False