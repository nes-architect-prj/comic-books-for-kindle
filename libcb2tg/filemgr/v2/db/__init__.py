from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from .schemas import Base
import os


# Database file
DATABASE_FILE="sqlite:///" + os.path.join(os.path.expanduser("~"), ".cb4k_cbt2g_db.sqlite")
engine = create_engine(url=DATABASE_FILE, echo=False)

# Make local session at import
Session = sessionmaker(bind=engine)

# Create DB if not exists
def init_db():
    Base.metadata.create_all(engine)

# Excuted at import
init_db()