from sqlalchemy import Column, UUID, DateTime, String, Integer, Boolean, ForeignKey
from sqlalchemy.orm import declarative_base, relationship
from sqlalchemy.sql import func
from .base import Base
import uuid


class Metas(Base):
    __tablename__ = "metas"

    # The ID of the meta will be an UUID
    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4())

    # Use later to implement Folder system
    is_dir = Column(Boolean, default=False)

    # Define the parent folder of this meta, if None is the attached to the root ones
    parent_dir = Column(UUID(as_uuid=True), ForeignKey("metas.id"), nullable=True, default=None)

    # Define name, type and size of the Original file
    file_name = Column(String, unique=True, nullable=False)
    file_type = Column(String, nullable=False, default="application/epub+zip")
    file_size = Column(Integer, nullable=False)

    # Define when the file has been created
    file_date = Column(DateTime(timezone=True), server_default=func.now())

    # Define if yes or no the parts are completes
    parts_complete = Column(Boolean, default=False)

    # Populate a parts attributes with associated parts to this Meta
    parts = relationship("Parts", back_populates="metadatas", cascade="all, delete")

    parent = relationship("Metas", remote_side=id, backref="sub_folders")


    def __repr__(self):
        return f"<Meta id={self.id} file_name={self.file_name} file_type={self.file_type} file_size={self.file_size} complete={self.parts_complete}>"

