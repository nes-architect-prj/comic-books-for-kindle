from sqlalchemy import Column, UUID, String, Integer, Boolean, ForeignKey
from sqlalchemy.orm import declarative_base, relationship
from .base import Base


class Parts(Base):
    __tablename__ = "parts"

    # Store Message and file IDs
    message_id = Column(Integer, primary_key=True)
    file_id = Column(String, nullable=False)

    # define the part number of the original file
    part_number = Column(Integer, nullable=False)

    # Foreign Key between Meta and Part
    meta_id = Column(UUID(as_uuid=True), ForeignKey("metas.id"), nullable=False)

    # Populate metadata attribute from the meta_id
    metadatas = relationship("Metas", back_populates="parts")

    def __repr__(self):
        return f"<Part id={self.message_id} file_id={self.file_id} meta_id={self.meta_id}>"
