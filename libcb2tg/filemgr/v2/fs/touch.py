import os
import magic
import requests
import json
from libcb2tg import creds
from libcb2tg.filemgr.v2 import fs
from libcb2tg.filemgr.v2.db import schemas, Session

def purge_from_tg(parts, debug:bool=False):
    """
    Quick function to purge existing parts on TG
    """
    token, chat_id = creds.load_credentials()
    tg_url =  f"https://api.telegram.org/bot{token}/deleteMessage"
    for part in parts:
        if debug: print(f"  ... deleting part from msg_id:{part.message_id}")
        tg_data = {"chat_id": chat_id, "message_id": part.message_id}
        tg_response = requests.post(url=tg_url, data=tg_data)
        if tg_response.status_code != 200: 
            if debug: print(f"      x Can't delete from TG... : {json.loads(tg_response.text)}")

def touch(file_path:str, force:bool=False, debug:bool=False):
    """
    Split the file if needed and upload each parts to the configured TG
    and store all file_id, message_id in database with Metas and Parts
    """

    # Real path
    file_path = os.path.expanduser(file_path)

    # Check if TG is correclty configured
    token, chat_id = creds.load_credentials()
    if not token or not chat_id:
        exit("Error: No TOKEN or CHAT_ID defined !")

    # First check if the file is not already present in DB and if it's complete
    session = Session()
    file_exists = session.query(schemas.Metas).filter_by(file_name=os.path.basename(file_path)).first()
    if file_exists:
        if not file_exists.parts_complete:
            if debug: 
                print(f"-> File exists but is not complete, purge it now...")
                session.delete(file_exists)
                # If some parts exists purge them from TG
                purge_from_tg(file_exists.parts, debug=debug)
                session.commit()
        else:
            if force:
                if debug: 
                    print(f"-> File exists but force to be recreated, purge it now...")
                    session.delete(file_exists)
                    # If some parts exists purge them from TG
                    purge_from_tg(file_exists.parts, debug=debug)
                    session.commit()
            else:
                raise Exception("-> File already exists") 

            

    # Open the file and read it entirely (Beawre for the memory!)
    with open( file_path , 'rb') as file:
        data = file.read()

    # Get its size
    file_size = len(data)
    num_parts = (file_size //fs.CHUNK_SIZE) + (1 if file_size % fs.CHUNK_SIZE > 0 else 0)

    if debug: print(f"-> Parts to be uploaded : {num_parts}")

    # Create MetaData
    metadatas = schemas.Metas(
        file_name=os.path.basename(file_path),
        file_type=magic.Magic(mime=True).from_file(file_path),
        file_size=file_size
    )
    
    # Now Save the metadata
    session.add(metadatas)
    session.commit()

    # Prepare the parts
    parts = []
    for i in range(num_parts):
        
        # Get the actual portion of data to be send to TG
        tg_data = data[i * fs.CHUNK_SIZE : (i + 1) * fs.CHUNK_SIZE]
        
        # Prepare to send to TG
        tg_files = {"document": tg_data}
        tg_data = {"chat_id": chat_id, "caption": f"{metadatas.file_name}.part.{i}"}
        
        # Send to TG
        if debug: print(f"-> Sending part {i+1}...")
        tg_response = requests.post(
            url=f"https://api.telegram.org/bot{token}/sendDocument",
            data=tg_data,
            files=tg_files
        )

        # Check the answer
        if tg_response.status_code == 200:
            document = tg_response.json().get("result").get("document")
            message_id = tg_response.json().get("result").get("message_id")
            # Create the part info and save it in DB
            part = schemas.Parts(
                part_number=i,
                metadatas=metadatas,
                file_id=document.get("file_id"),
                message_id=message_id
            )
            session.add(part)
            # Save the part to DB
            session.commit()
        else:
            print(json.loads(tg_response.text))
            raise Exception(f"Can't upload part n°{i} to TG...")
        

    # Update metadatas
    metadatas.parts_complete = True
    session.add(metadatas)
    session.commit()

    if debug: print(metadatas)

    # Return the created Meta object
    return metadatas