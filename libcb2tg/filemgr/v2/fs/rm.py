from libcb2tg.filemgr.v2.db import schemas, Session
from libcb2tg import creds
import requests
import json


def rm(file:schemas.Metas, debug:bool=False):
    """
    Remove a file from local db and his data from TG
    """

    # Check if TG is correclty configured
    token, chat_id = creds.load_credentials()
    if not token or not chat_id:
        exit("Error: No TOKEN or CHAT_ID defined !")

    # Init db Session
    session = Session.object_session(file) if Session.object_session(file) is not None else Session()

    if debug: print(f"> deleting {file.file_name}")

    # Remove from TG first
    token, chat_id = creds.load_credentials()
    tg_url =  f"https://api.telegram.org/bot{token}/deleteMessage"
    for part in file.parts:
        if debug: print(f"  ...deleting part from msg_id:{part.message_id}")
        tg_data = {"chat_id": chat_id, "message_id": part.message_id}
        tg_response = requests.post(url=tg_url, data=tg_data)
        if tg_response.status_code != 200: 
            if debug: print(f"      [!] Can't delete from TG... : {json.loads(tg_response.text)}")
            session.delete(part)
        else:
            session.delete(part)
        session.commit()

    # Update file as no parts
    file.parts_complete = False
    session.add(file)
    session.commit()

    # Remove file from DB
    session.delete(file)
    session.commit()

    session.close()

    # Return the deleted file
    return file
    
