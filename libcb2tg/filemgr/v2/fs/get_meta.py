from libcb2tg.filemgr.v2.db import Session
from libcb2tg.filemgr.v2.db.schemas import Metas


def get_meta(meta_id):
    """
    Return the metadata of a stored file
    """


    session = Session()
    meta = session.query(Metas).filter_by(id=meta_id).first()

    return meta