from .list import list
from .get_meta import get_meta
from .touch import touch
from .mkdir import mkdir
from .rm import rm

# TG Bot size limit to 20M
CHUNK_SIZE = 20*1024*1024