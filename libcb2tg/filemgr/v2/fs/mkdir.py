from libcb2tg.filemgr.v2.db import Session
from libcb2tg.filemgr.v2.db import schemas 
from typing import Union


def mkdir(dir_name:str, parent_dir:Union[schemas.Metas, None]=None):
    """
    Create a folder and attach it to his parent, if no parent, attached to the root one
    If already exist return None, else return the created Folder as Meta object
    """
    
    # Open session DB
    session = Session()

    # Check if the folder in the parent didn't exists
    dir_exists = session.query(schemas.Metas).filter_by(parent_dir=None if parent_dir is None else parent_dir.id, file_name=dir_name, is_dir=True).first()
    if dir_exists is not None:
        return dir_exists

    # Create the folder meta object
    dir_created = schemas.Metas(
        is_dir=True,
        parent_dir=None if parent_dir is None else parent_dir.id,
        file_name=dir_name,
        file_size=4096,
        file_type="dir",
        parts_complete=True
    )
    session.add(dir_created)

    # Commit
    session.commit()

    return dir_created