from libcb2tg.filemgr.v2.db import Session
from libcb2tg.filemgr.v2.db.schemas import Metas


def list(parent_dir=None):
    """
    List folder and files stored on the ChatID
    """
    session = Session()
    metas = session.query(Metas).filter_by(parent_dir=parent_dir)

    return metas.all()