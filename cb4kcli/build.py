import tempfile
import os
import shutil
import libcb4k, libcb2tg, libcbmail

def build_fct(args):
    
    TMP_FOLDER=tempfile.mkdtemp()

    # Cleanup the work folder
    if os.path.exists(TMP_FOLDER): shutil.rmtree(TMP_FOLDER, ignore_errors=True)
    
    print(f" [*] Building EPUB from '{args.file}' :")
    print(f"    > Temp folder for building : {TMP_FOLDER} ...")

    # Extract the images
    libcb4k.cb_extract(cbfile=args.file, output=os.path.join(TMP_FOLDER, "images"), debug=args.debug)

    # Compress the images
    if args.max_size > 0: print(f"    > Process and compress images to fit {round(args.max_size / 1000 / 1000)}MB ...")
    libcb4k.cb_convert_img_folder(folder=os.path.join(TMP_FOLDER, "images"), max_size=args.max_size, quality=args.quality, double_page=args.double_pages, debug=args.debug)

    # Create the EPUB object and specify the images folder
    epub = libcb4k.Epub(image_folder=os.path.join(TMP_FOLDER, "images.converted"), temp_folder=TMP_FOLDER)

    # Define the info of the ebook
    epub.title = os.path.splitext( os.path.basename(args.file) )[0]

    # Build all contents (chapters, xml files...)
    epub.build()

    # Create the zipped EPUB
    libcb4k.cb_compress(epub=epub, output_folder=args.output)

    # Path for the EPUB file built
    epub_file = os.path.join(args.output, epub.title + ".epub")

    # save to TG ?
    if args.send_to_tg :
        # First remove file if exists
        file_exists = libcb2tg.filemgr.v1.filedb.find_save_file(file_name=epub.title + ".epub")
        if file_exists is not None:
            if args.debug: print("   ... exists, removing old one")
            libcb2tg.filemgr.v1.delete(file_id=file_exists.get("file_id"))

        # send
        print(f"    > Sending to TG")
        libcb2tg.filemgr.v1.upload(file=epub_file)

    # Send to mail ?
    if args.send_to_mail :
        print(f"    > Sending by mail")
        libcbmail.send(file_path=epub_file, debug=args.debug)

    print(f"    > Build done")

def build(args):

    try:
        build_fct(args)
    except Exception as e:
        raise e
