import libcb2tg


def telegram_download(args):
    """
    Download EPUB from file ID
    """

    # First retrive the file from his ID
    download_file = libcb2tg.filemgr.v1.filedb.find_save_file(args.file_name)
    if download_file is None:
        raise Exception(f"File {args.file_name} not found !")

    # Download the file
    libcb2tg.bot.filemgr.download(file_id=download_file.get("file_id"), output_path=args.output)

