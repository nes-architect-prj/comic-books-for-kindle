import libcb2tg


def telegram_rm(args):
    """
    Remove EPUB from file ID on the Telegram side
    """

    # First retrive the file from his ID
    remove_file = libcb2tg.filemgr.v1.filedb.find_save_file(args.file_name)
    if remove_file is None:
        raise Exception(f"File {args.file_name} not found !")

    # Remove the file
    if not libcb2tg.filemgr.v1.delete(file_id=remove_file.get("file_id")):
        if args.force:
            print("Error happen, but --force tag found, remove entry from local db...")
            libcb2tg.filemgr.v1.filedb.delete_file_id(remove_file.get("file_id"))
