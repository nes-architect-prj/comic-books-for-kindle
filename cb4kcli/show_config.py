import libcb2tg, libcbmail
import json

def show_config(args):

    config = {
        "telegram": libcb2tg.creds.load_credentials(to_dict=True),
        "mail": libcbmail.creds.load_credentials(to_dict=True)
    }

    print( json.dumps(config, indent=4) )