import libcbmail


def config_mail(args):
    """
    Configure the mail settings
    """
    libcbmail.creds.save_credentials(
        smtp_server=args.smtp_server,
        smtp_from=args.smtp_from,
        smtp_pass=args.smtp_pass,
        smtp_port=args.smtp_port,
        smtp_to=args.smtp_to
    )

    print(f"Mail settings saved to {libcbmail.creds.CONFIG_FILE}")