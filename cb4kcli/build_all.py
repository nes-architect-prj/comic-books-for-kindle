import os
from cb4kcli import build_fct

def build_all(args):
    
    # get all CBZ/CBR/PDF files
    for file in [entry for entry in os.listdir(path=args.folder) if entry.endswith( ('.cbz', '.cbr', '.pdf') )]:
        args.file = file
        build_fct(args)

    print("[*] Convert all files done")
