import libcb2tg
from datetime import datetime
from prettytable import PrettyTable


def telegram_list(args):
    """
    List files stored on the TG Chat ID
    """

    filter : str = ".epub" if args.filter_list == "*" else args.filter_list

    table = PrettyTable(["Date","ID","Name","Size"])
    table.align = "l"
    for file in [file for file in libcb2tg.filemgr.v1.filedb.list_saved_files() if filter != "*" and filter in file.get("file_name")]:
        table.add_row([
            datetime.fromtimestamp(file.get('file_date')),
            file.get('file_id'),
            file.get('file_name'),
            f"{round(file.get('file_size')/1024/1024, 2)}M"
        ])
    
    print(table) if len(table.rows) > 0 else print("No file is present on the TG chat")