import argparse
import os
import cb4kcli.telegram_rm
import libcb4k, libcb2tg, libcbmail
import cb4kcli
from libcb4k import DPTraitment
from typing import Union


def main():
    """
    ?
    """
    
    # The main parser
    parser = argparse.ArgumentParser(prog="cb4kcli", description="CB4K - Native EPUB Builder for Comics Books")
    subparser = parser.add_subparsers(metavar="")

    # Config Mail Parser 
    config_mail_p = subparser.add_parser("set-mail", help="Configure the mail settings")
    config_mail_p.add_argument("-s", "--smtp-server", help="SMTP Server", required=True)
    config_mail_p.add_argument("-p", "--smtp-port", type=int, help="SMTP Port", required=True)
    config_mail_p.add_argument("-f", "--smtp-from", help="SMTP From address", required=True)
    config_mail_p.add_argument("-t", "--smtp-to", help="SMTP To address", required=True)
    config_mail_p.add_argument("-u", "--smtp-user", help="SMTP From username", required=True)
    config_mail_p.add_argument("-x", "--smtp-pass", help="SMTP From password", required=True)
    config_mail_p.set_defaults(func=cb4kcli.config_mail)

    # Config TG Parser 
    config_telegram_p = subparser.add_parser("set-telegram", help="Configure the telegram settings")
    config_telegram_p.add_argument("-t", "--token", help="API Token", required=True)
    config_telegram_p.add_argument("-c", "--chat-id", help="Chat ID", required=True)
    config_telegram_p.set_defaults(func=cb4kcli.config_telegram)

    # Config reader
    config = subparser.add_parser("show-config", help="Display the saved configuration for TG and Mail")
    config.set_defaults(func=cb4kcli.show_config)

    # Build Parser
    build_pa = subparser.add_parser("build", help="Build EPUB 3.3 ebooks from supported files (cbz,cbr or pdf)")
    build_pa.add_argument("-f", "--file", help="A Comic Book file in compatible format (cbz, cbr or pdf)", required=True)
    build_pa.add_argument("-o", "--output", help="Output folder of builded EPUB (default was current dir)", default=os.getcwd())
    build_pa.add_argument("-q", "--quality", type=int, help="JPEG quality for compressed images (default was 50)", default=80)
    build_pa.add_argument("-s", "--max-size", type=int, help="The target size of the EPUB in Bytes, will be lower or equal to this value (default was 50MB)", default=(50*1000*1000))
    build_pa.add_argument("-d", "--double-pages", type=DPTraitment, help="Define how double pages must be processed (SPLIT_RTL ,SPLIT_LTR, ROTATE, NOTHING), default is NOTHING", default=DPTraitment.NOTHING)
    build_pa.add_argument("-t", "--send-to-tg", help="send the EPUB to the configured Telegram Bot Chat ID", action="store_true")
    build_pa.add_argument("-m", "--send-to-mail", help="send the EPUB to the configured Mail account (be aware of smtp limit size)", action="store_true")
    build_pa.add_argument("--debug", help="Enable verbose mode", action="store_true")
    build_pa.set_defaults(func=cb4kcli.build)

    # Build all
    build_all_pa = subparser.add_parser("build-all", help="Build EPUB 3.3 ebooks from folder within supported files (cbz,cbr or pdf)")
    build_all_pa.add_argument("-f", "--folder", help="A folder with Comic Book file int it in compatible format (cbz, cbr or pdf)", default=os.getcwd())
    build_all_pa.add_argument("-o", "--output", help="Output folder of builded EPUB (default was current dir)", default=os.getcwd())
    build_all_pa.add_argument("-q", "--quality", type=int, help="JPEG quality for compressed images (default was 50)", default=80)
    build_all_pa.add_argument("-s", "--max-size", type=int, help="The target size of the EPUB in Bytes, will be lower or equal to this value (default was 50MB)", default=(49*1000*1000))
    build_all_pa.add_argument("-d", "--double-pages", type=DPTraitment, help="Define how double pages must be processed (SPLIT_RTL ,SPLIT_LTR, ROTATE, NOTHING), default is NOTHING", default=DPTraitment.NOTHING)
    build_all_pa.add_argument("-t", "--send-to-tg", help="send the EPUB to the configured Telegram Bot Chat ID", action="store_true")
    build_all_pa.add_argument("-m", "--send-to-mail", help="send the EPUB to the configured Mail account (be aware of smtp limit size)", action="store_true")
    build_all_pa.add_argument("--debug", help="Enable verbose mode", action="store_true")
    build_all_pa.set_defaults(func=cb4kcli.build_all)

    # Telegram commands
    telegram_pa = subparser.add_parser("telegram-list", help="Telegram commands for listing uploaded EPUB, by default list all EPUB")
    telegram_pa.add_argument("-f","--filter-list", type=str, help="Filter epub Name", default="*")
    telegram_pa.set_defaults(func=cb4kcli.telegram_list)

    telegram_dl_pa = subparser.add_parser("telegram-dl", help="Download an EPUB from Telegram by his Name")
    telegram_dl_pa.add_argument("-f", "--file-name", type=str, help="Name of the EPUB retrieve by the telegram-list command", required=True)
    telegram_dl_pa.add_argument("-o", "--output", type=str, help="Output filename of the downloaded file", required=True)
    telegram_dl_pa.set_defaults(func=cb4kcli.telegram_download)

    telegram_rm_pa = subparser.add_parser("telegram-rm", help="Remove an EPUB from Telegram by his Name")
    telegram_rm_pa.add_argument("-f", "--file-name", type=str, help="Name of the EPUB retrieve by the telegram-list command", required=True)
    telegram_rm_pa.add_argument("-F", "--force", help="Force remove from local DB even Telegram respond with error", action="store_true")
    telegram_rm_pa.set_defaults(func=cb4kcli.telegram_rm)

    try:
        args = parser.parse_args()
        args.func(args)
    except Exception as e:
        raise e
        print(parser.print_help())

if __name__ == "__main__":
    main()