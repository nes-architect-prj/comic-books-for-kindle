import libcb2tg


def config_telegram(args):
    """
    Configure the TG settings
    """
    libcb2tg.creds.save_credentials(
        token=args.token,
        chat_id=args.chat_id
    )

    print(f"Telegram settings saved to {libcb2tg.creds.CONFIG_FILE}")