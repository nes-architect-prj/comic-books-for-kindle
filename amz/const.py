# Constants for LWA features

_LWA_URI_ROOT = "https://api.amazon.fr"

_LWA_URI_DEVICE_AUTH_REQUEST = "/auth/o2/create/codepair"
_LWA_URI_TOKEN = "/auth/o2/token"

_LWA_URI_PROFILE = "/user/profile"

_CLIENT_APP_NAME = "CB4K_PIP"
_CLIENT_APP_ID = "amzn1.application-oa2-client.dc8c88942f6f4169883b7ab0d3a17969"