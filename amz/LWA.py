"""
LWA : Login With Amazon 

Implementation of the logic to Login with Amazon to get Customer profile over the oauth mech of amazon

Lot of help : 
    https://developer.amazon.com/fr/docs/login-with-amazon/retrieve-token-other-platforms-cbl-docs.html
"""

import requests
from typing import Union
import json
import os
from amz import const
import time


class DeviceAuthRequest:
    """
    The request format to query a device_code
    """
    response_type: str
    client_id: str
    scope: str
    def __init__(self, client_id:str, response_type:str = "device_code", scope:str = "profile" ):
        self.client_id = client_id
        self.response_type = response_type
        self.scope = scope

class DeviceAuthResponse:
    """
    The API response format for device_code query
    """
    user_code: str
    device_code: str
    verification_uri: str
    expires_in: int
    interval: int
    def __init__(self, user_code:str,device_code:str,verification_uri:str,expires_in:int,interval:int):
        self.user_code = user_code
        self.device_code = device_code
        self.verification_uri = verification_uri
        self.expires_in = expires_in
        self.interval = interval

class DeviceAuthToken:
    """
    The device token format received after device registered
    """
    access_token:str
    refresh_token:str
    token_type:str
    expires_in:int
    def __init__(self, access_token:str,refresh_token:str,expires_in:int):
        self.access_token = access_token
        self.refresh_token = refresh_token
        self.token_type = "bearer"
        self.expires_in = expires_in

class CustomerProfileResponse:
    """
    The Customer profile format received after query the profile endpoint
    link: https://developer.amazon.com/fr/docs/login-with-amazon/customer-profile-other-platforms-cbl-docs.html
    """
    user_id: str
    email: str
    name: str
    postal_code: str
    def __init__(self, user_id:str, email:str, name:str, postal_code:str):
        self.user_id = user_id
        self.email = email
        self.name = name
        self.postal_code = postal_code

def pair_device(device_req: DeviceAuthRequest = DeviceAuthRequest(client_id=const._CLIENT_APP_ID)) -> DeviceAuthResponse:
    """
    Ask for device_code from a device auth request
    """

    response = requests.post(
        url=f"{const._LWA_URI_ROOT}{const._LWA_URI_DEVICE_AUTH_REQUEST}",
        data=device_req.__dict__
    )

    if response.status_code != 200:
        raise Exception(response.text)

    response = json.loads(response.text)
    
    return DeviceAuthResponse(
        user_code = response.get("user_code"),
        device_code = response.get("device_code"),
        interval = response.get("interval"),
        verification_uri = response.get("verification_uri"),
        expires_in = response.get("expires_in"),
    )

def get_access_refresh_tokens(codes:DeviceAuthResponse) -> DeviceAuthToken:
    """
    Obtain Access and refresh tokens against device_code
    """

    response = requests.post(
        url=f"{const._LWA_URI_ROOT}{const._LWA_URI_TOKEN}",
        data={
            "grant_type": "device_code",
            "device_code": codes.device_code,
            "user_code": codes.user_code
        }
    )

    if response.status_code != 200:
        raise Exception(response.text)
    
    response = json.loads(response.text)

    return DeviceAuthToken(
        access_token=response.get("access_token"),
        refresh_token=response.get("refresh_token"),
        expires_in=int(response.get("expires_in")),
    )

def get_profile(tokens: Union[DeviceAuthToken, None] = None) -> CustomerProfileResponse:
    """
    Return the customer profile with valid tokens only
    By default load the stored tokens, else load the specified token in arg
    """
    response = requests.get(
        url=f"{const._LWA_URI_ROOT}{const._LWA_URI_PROFILE}",
        headers={
            "Authorization": f"Bearer {token_loads().access_token if tokens is None else tokens.access_token}"
        }
    )

    if response.status_code != 200:
        raise Exception(response.text)
    
    response = json.loads(response.text)

    return CustomerProfileResponse(
        user_id=response.get("user_id"),
        email=response.get("email"),
        name=response.get("name"),
        postal_code=response.get("postal_code"),
    )


def renew_tokens(tokens : DeviceAuthToken) -> DeviceAuthToken:
    """
    Renew the expired tokens with the refresh one
    """
    response = requests.post(
        url=f"{const._LWA_URI_ROOT}{const._LWA_URI_TOKEN}",
        data={
            "grant_type": "refresh_token",
            "refresh_token": tokens.refresh_token,
            "client_id": const._CLIENT_APP_ID
        }
    )

    if response.status_code != 200:
        raise Exception(response.text)
    
    response = json.loads(response.text)

    return DeviceAuthToken(
        access_token=response.get("access_token"),
        refresh_token=response.get("refresh_token"),
        expires_in=int(response.get("expires_in")),
    )

def token_loads() -> DeviceAuthToken:
    """
    Load DeviceAuthToken from json file ~/.lwa_tokens
    If the loaded token is too old, renew them automatically
    """
    
    token_file = os.path.join( os.path.expanduser("~"), ".lwa_tokens")

    with open( token_file , "r" ) as file:
    
        file_tokens = json.load(file)
        device_tokens = DeviceAuthToken(
            access_token=file_tokens.get("access_token"),
            refresh_token=file_tokens.get("refresh_token"),
            expires_in=int(file_tokens.get("expires_in")),
        )

        # Check if the access token is always valid
        current_time= time.time()

        # Get the latest edit fo the token file
        last_mod_time = os.path.getmtime(token_file)

        # Diff time and renew the token if old than 1hr
        time_diff = current_time - last_mod_time
        if time_diff >= 3600:
            print(f"Renewing the tokens because is {round(time_diff-3600)}s too old")
            device_tokens = renew_tokens(device_tokens)
            token_dump(device_tokens)
        else:
            # Can be removed after testing
            print(f"The access tokens is valid for {round(3600-time_diff)}s yet")


        return device_tokens

def token_dump(tokens: DeviceAuthToken):
    """
    Save DevcieAuthToken in json file ~/.lwa_tokens
    """
    with open( os.path.join( os.path.expanduser("~"), ".lwa_tokens"), "w+" ) as file:
        file.write(json.dumps(tokens.__dict__, indent=4))