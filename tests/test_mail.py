import libcbmail

# Save credentials to Home folder
# GMAIL allow 25MB attachement, but Amazon allow to up to 50MB by email
# If you have personnal mail server, prefer it to bypass the 25MB limitation and grow your ebook quality up to 50MB !

# libcbmail.creds.save_credentials(
#     smtp_from="***@gmail.com",
#     smtp_pass="***",
#     smtp_port=465,
#     smtp_server="smtp.gmail.com",
#     smtp_to="***@kindle.com"
# )

# Load creds
creds = libcbmail.creds.load_credentials()

# Send test message
libcbmail.send("./Pipfile", debug=True)