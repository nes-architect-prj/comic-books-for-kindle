import libcb4k
import shutil
import os

# Temp folder
output = "./tmp"

# Cleanup the work folder
if os.path.exists(output): shutil.rmtree(output, ignore_errors=True)
else: os.mkdir(output)

# Extract the images
libcb4k.cb_extract(cbfile="test.cbz", output=os.path.join(output, "images"))

# Compress the images
libcb4k.cb_convert_img_folder(folder=os.path.join(output, "images"), max_size=20971520)

# Create the EPUB object and specify the images folder
epub = libcb4k.Epub(image_folder=os.path.join(output, "images.converted"))

# Define the info of the ebook
epub.title = "Cat's Eyes - T01"

# Build all contents (chapters, xml files...)
epub.build()

# Create the zipped EPUB
libcb4k.cb_compress(epub=epub)