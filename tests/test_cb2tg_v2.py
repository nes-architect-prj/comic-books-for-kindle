from libcb2tg.filemgr.v2.db.schemas import Metas, Parts
from libcb2tg.filemgr.v2.db import Session
from libcb2tg.filemgr.v2.fs import list, get_meta, touch, mkdir, rm

def test_db():
    """
    Test the creation of items in DB
    """

    # Create an sql object in memeory first
    meta = Metas(
        file_name="Test.epub",
        file_size=20*1024*1024
    )

    # Create parts for this Meta item
    parts = [
        Parts(
            message_id=1,
            file_id="something1",
            part_number=1,
            metadatas=meta
        ),
        Parts(
            message_id=2,
            file_id="something2",
            part_number=2,
            metadatas=meta
        ),
    ]

    # Save it in DB
    # session = Session()
    # session.add(meta, parts)
    # session.commit()

    # Print 
    print(meta.parts)

def test_list():
    """
    List existing files in DB
    """
    for entry in list():
        print(f"[{'DIR ' if entry.is_dir else 'FILE'}] {entry.file_name} [subfolder(s) : {len(entry.sub_folders)}]")

def test_get_meta():
    """
    Display info on a file
    """
    entries = list()
    if entries is None:
        raise Exception("No files for this test has been found...")

    meta = get_meta(meta_id=entries.pop(0).id)
    if meta is None:
        print(f"No meta found ")
    else:
        print(meta.parts)
        print(f"Filename: {meta.file_name}, size:{meta.file_size}, nb_parts:{len(meta.parts)}")

def test_touch():
    """
    Try to spliut and upload file, to TG and save metas and parts info in local DB
    """
    touch("~/sunshine.AppImage", debug=True, force=True)

def test_mkdir():
    """
    Try to create folder in DB
    """
    mkdir(dir_name="test 5")

def test_submkdir():
    """
    Try to create folder in DB into the first existing folder
    """
    session = Session()
    parent_dir = session.query(Metas).filter_by(is_dir=True).first()
    if parent_dir is not None:
        mkdir(dir_name="Sub folder", parent_dir=parent_dir)

def test_rm():
    """
    Try to upload and remove a file 
    """

    upload = touch("~somefile", debug=True)
    print(f"{upload.file_name} uploaded !!")

    removed = rm(file=upload, debug=True)
    print(f"{removed.file_name} as been removed !!")

if __name__ == "__main__":
    # test_db()
    # test_touch()
    test_list()
    # test_get_meta()
    # test_mkdir()
    # test_submkdir()
    # test_rm()