import libcb2tg
import asyncio


# Load credentials
token, chat_id = libcb2tg.creds.load_credentials()

# Check if we can discuss with the chat
libcb2tg.creds.test_credentials()

# Upload a file
new_file = libcb2tg.filemgr.upload(file="Cat's Eyes - T01.epub")
print(f"[*] {new_file.get('file_name')} has been uploaded !")

# List files 
print("[*] Listing of the saved files :")
for file in libcb2tg.filemgr.filedb.list_saved_files():
    print(f"    - {file.get('file_name')} [ID:{file.get('file_id')}]")

# Download the file
print(f"[*] File {new_file.get('file_id')} is downloading ...")
libcb2tg.filemgr.download(file_id=new_file.get("file_id"))
print(f"[*] File {new_file.get('file_id')} downloaded to ~")

# Delete the file
libcb2tg.filemgr.delete(file_id=new_file.get("file_id"))
print(f"[*] File {new_file.get('file_id')} deleted from DB and TG")
