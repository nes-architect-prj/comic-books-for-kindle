from . import creds
from email import encoders, utils
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from typing import Union
import smtplib
import ssl
import os


def send(file_path:str, to:Union[str, None]=None, debug:bool=False):
    """
    Send file to email, if to not defined it will used the saved "smtp_to" setting saved
    """

    # Load settings
    smtp_server, smtp_port, smtp_from, smtp_pass, smtp_to = creds.load_credentials()
    if debug: print("SMTP settings loaded")

    # FileName
    file_name = os.path.basename(file_path)

    # Prepare the EPUB to be attached
    with open(file_path, "rb") as epub_file:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(epub_file.read())
    encoders.encode_base64(part)
    part.add_header("Content-Disposition", f"attachment; filename={file_name}")
    if debug: print(f"SMTP file attachment preapred : {file_name}")

    # Prepare the message
    msg = MIMEMultipart()
    msg["Subject"] = f"CB4K : Nouvel EPUB '{file_name}'"
    msg["From"] = smtp_from
    msg["To"] = smtp_to if to is None else to
    msg["Date"] = utils.formatdate(localtime=True)
    msg["Message-ID"] = utils.make_msgid()

    msg.attach(MIMEText("Lovely sent by CB4K..."))
    msg.attach(part)
    if debug: print("SMTP message built")

    # Send the mail
    with smtplib.SMTP_SSL(smtp_server, smtp_port) as server:
       server.login(smtp_from, smtp_pass)
       if debug: print(f"SMTP login in on {smtp_server}:{smtp_port}")
       result = server.sendmail(smtp_from, smtp_to if to is None else to, msg.as_string())
       if debug: print(f"SMTP message sent to {msg['to']}")
       if debug: print(f"SMTP message result ", result)
