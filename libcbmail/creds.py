import os
import json
import requests


CONFIG_FILE = os.path.join( os.path.expanduser("~"), ".cb4k_mail")

def save_credentials(smtp_server:str, smtp_port:int, smtp_from:str, smtp_pass:str, smtp_to:str):
    """
    Save smtp settings
    """
    data =  {
        "smtp_server": smtp_server,
        "smtp_port": smtp_port,
        "smtp_from": smtp_from,
        "smtp_pass": smtp_pass,
        "smtp_to": smtp_to
    }

    with open(CONFIG_FILE, "w") as file:
        json.dump(data, file, indent=4)

def load_credentials(to_dict:bool=False):
    """
    Load credentials from json file
    """
    try:
        with open(CONFIG_FILE, "r") as file:
            data = json.load(file)
            return data if to_dict else (data["smtp_server"], data["smtp_port"], data["smtp_from"], data["smtp_pass"], data["smtp_to"])
    except FileNotFoundError:
        print(f"Config file {CONFIG_FILE} not found !")
        return {} if to_dict else (None, None)
    except KeyError:
        print(f"File {CONFIG_FILE} is corrupted or not a json")

def test_credentials():
    """
    Test SMTP settings
    """
    pass